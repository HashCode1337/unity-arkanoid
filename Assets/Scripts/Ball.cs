using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    // sphere collider
    private SphereCollider _sc;
    private Rigidbody _rb;
   
    // ball direction
    private float _dirX;
    private float _dirY;
    private float _speed;
    
    // is game object has collision/rigidBody
    private bool _bHasCollision;
    private bool _bHasRigitBody;

    // game master to communicate with
    private GameMaster _gm;

    // bonus or main ball flag, bonus ball doesn't subtract player lives count when dies
    private bool _bIsMainBall = true;

    void Start()
    {
        // get game master
        _gm = FindObjectOfType<GameMaster>();
        
        // assign main vars
        _dirX = _gm.startBallDirection.x;
        _dirY = _gm.startBallDirection.y;
        _speed = _gm.ballSpeed;
        
        // check collision component and rigidBody
        _bHasCollision = TryGetComponent<SphereCollider>(out _sc);
        _bHasRigitBody = TryGetComponent<Rigidbody>(out _rb);

        // disable if invalid _sc
        if (!_bHasCollision)
        {
            Debug.Log("Error - Ball doesnt have collision component and will be disabled");
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // If ball outside of play area bounds, delete it
        if (Vector3.Distance(_gm.transform.position, transform.position) > 30) Destroy(gameObject);
        
        gameObject.transform.position = gameObject.transform.position + new Vector3(_dirX, 0, _dirY).normalized * (Time.fixedDeltaTime * _speed);
    }

    private void OnCollisionEnter(Collision other)
    {
        
        // call gamemode ball dead logic
        if (other.gameObject.CompareTag("KillZone"))
        {
            _gm.RegisterBallDead(this);
        }

        // get hitpoint
        Vector3 hitPoint = new Vector3();
        foreach (ContactPoint cp in other.contacts)
        {
            hitPoint.x = cp.point.x;
            hitPoint.z = cp.point.z;
        }

        // trace logic to detect reflection
        Ray ray = new Ray(gameObject.transform.position, hitPoint);
        RaycastHit rch;
        Physics.Raycast(ray, out rch, 5);
        
        // write reflection value into ball dir
        Vector3 reflectedDir = Vector3.Reflect(new Vector3(_dirX, 0 , _dirY), rch.normal);
        _dirX = reflectedDir.x;
        _dirY = reflectedDir.z;

    }

    public void SetMainBall(bool bIsMain)
    {
        _bIsMainBall = bIsMain;
    }

    public bool GetBallIsMain()
    {
        return _bIsMainBall;
    }
}
