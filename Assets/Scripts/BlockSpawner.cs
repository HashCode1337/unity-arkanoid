using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    // take its pos to build spawn grid
    // row spawn dir -> from left to right
    // column spawn dir -> from up to down
    public GameObject firstBlock;
    
    public int rows = 6;
    public int columns = 9;
    
    // offset between blocks
    public float offset = 1;

    // for offset calculations
    private Vector3 _currentSpawnPos;

    public void SpawnBlocks()
    {
        // read master block position
        _currentSpawnPos = firstBlock.gameObject.transform.position;

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                Vector3 newPos = new Vector3(_currentSpawnPos.x + i * offset, _currentSpawnPos.y, _currentSpawnPos.z - j * offset);
                Quaternion newDir = firstBlock.transform.rotation;

                GameObject newBlock = Instantiate(firstBlock, newPos, newDir);
                newBlock.SetActive(true);
            }
        }
    }
}
