using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    // Speed
    public float bonusFallingSpeed;
    
    // Trigger box
    private BoxCollider _cb;
    
    // Game master
    private GameMaster _gm;
    
    void Start()
    {
        _cb = GetComponent<BoxCollider>();
        _gm = FindObjectOfType<GameMaster>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(1,1,1, Space.Self);
        transform.Translate(0,0,- bonusFallingSpeed * Time.fixedDeltaTime,Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            _gm.SpawnBonusBall();
        }
        else if (other.CompareTag("KillZone"))
        {
            Destroy(gameObject);
        }
    }
}
