using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Custom struct to expose KeyPair to inspector
public class HitColorMap : ScriptableObject
{
    [System.Serializable]
    public class HitColorEntry
    {
        public int hitNumber;
        public Color color;
    }
}
// end

public class Destructible : MonoBehaviour
{
    public List<HitColorMap.HitColorEntry> hitColorsMap;
    
    private int hits = 0;
    private int maxHits;
    private GameMaster gm;
    
    void Start()
    {
        // calc max possible hits as colors count in array
        maxHits = hitColorsMap.Count;

        // check if lives not set
        if (maxHits <= 0)
        {
            Debug.LogError("LOG DESTRUCTIBLE: hitColorsMap unrepresented! Fill at least one key-pair value, or block will have 0 health");
        }
        
        gm = FindObjectOfType<GameMaster>();
    }

    private void OnCollisionEnter(Collision other)
    {
        // if hitten by ball, apply changes
        if (other.gameObject.CompareTag("Ball"))
        {
            ApplyHit();
        }
    }

    public void ApplyHit()
    {
        // check if lives ran out
        if (hits >= maxHits)
        {
            gameObject.SetActive(false);
            gm.RegisterCubeDestroyed(gameObject);
            return;
        }

        // get current hit number and its color
        HitColorMap.HitColorEntry curColor = hitColorsMap[hits];
        // apply
        GetComponent<Renderer>().material.color = curColor.color;

        //register hit
        hits++;
    }
}
