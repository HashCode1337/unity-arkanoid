using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;


public class GameMaster : MonoBehaviour
{
    // exposed values
    public Vector2 startBallDirection = Vector2.one;
    public float ballSpeed = 10;
    public float mouseSpeedMultiplier = 10.0f;
    public int startPlayerLives = 3;
    public int bonusChance = 10;

    // exposed objs
    public Text livesCounterUI;
    public Text cubesCounterUI;
    public GameObject playerPlatform;
    public GameObject playerBall;
    public GameObject[] bonusTypes;
    
    // private vars
    private GameObject _playerBallPrefab;
    private int _lives;
    private int _cubesDestroyed;
    private Canvas _rootCanvas;
    private Canvas _gameplayCanvas;
    private Canvas _menuCanvas;
    private TouchHandle _touchHandle;
    private GameObject _deathWall;
    private BlockSpawner _blockSpawner;

    void Start()
    {
        // Hold ball prefab
        _playerBallPrefab = playerBall;
        
        // set player lives count and refresh ui
        _lives = startPlayerLives;
        UpdateUI();
        
        // Search scripted root canvas
        foreach (Canvas canvasElement in FindObjectsOfType<Canvas>())
            if (canvasElement.isRootCanvas)
                _rootCanvas = canvasElement;

        // Get gameplay canvas via tag
        _gameplayCanvas = GameObject.FindWithTag("UIGamePlay").GetComponent<Canvas>();
        _gameplayCanvas.enabled = false;
        
        // Get menu canvas and show it
        _menuCanvas = GameObject.FindWithTag("UIMenu").gameObject.GetComponent<Canvas>();
        _menuCanvas.enabled = true;
        
        // Ball kill area
        _deathWall = GameObject.FindWithTag("KillZone");
        
        // Find block spawner and put into var
        _blockSpawner = GameObject.FindWithTag("Spawner").gameObject.GetComponent<BlockSpawner>();
        
        // Get TouchHandle instance
        _touchHandle = _rootCanvas.GetComponent<TouchHandle>();
        _touchHandle.InitSettings(_deathWall, playerPlatform, mouseSpeedMultiplier);
        
        StartGame(true, "Welcome!");
    }

    public void UI_StartGame()
    {
        StartGame(false, "");
    }

    public void UI_ExitGame()
    {
        Application.Quit();
    }
    
    public void StartGame(bool aiMode, String menuText)
    {
        // Init new game
        _lives = startPlayerLives;
        _cubesDestroyed = 0;

        // Clear all blocks if they are exist
        GameObject[] blocks = GameObject.FindGameObjectsWithTag("Destructable");
        foreach (GameObject block in blocks) Destroy(block);

        // Clear all balls in scene
        GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        foreach (GameObject ball in balls) Destroy(ball);
        
        // Find block spawner and call spawn
        _blockSpawner.SpawnBlocks();

        // put plate on start position
        playerPlatform.transform.position = new Vector3(0, 0.5f, -14);
        
        // Spawn ball
        SpawnBall(_playerBallPrefab, true);
        
        // Enable ai if it is preview
        if (aiMode)
        {
            _menuCanvas.enabled = true;
            _gameplayCanvas.enabled = false;
            _touchHandle.SetAIMode(true);
            
            // Set title text in main menu
            _menuCanvas.GetComponentInChildren<Text>().text = menuText;
        }
        else
        {
            _gameplayCanvas.enabled = true;
            _menuCanvas.enabled = false;
            _touchHandle.SetAIMode(false);
        }
    }

    private void UpdateUI()
    {
        // just ui updates
        livesCounterUI.text = _lives.ToString();
        cubesCounterUI.text = _cubesDestroyed.ToString();
    }

    private void SpawnBonus(Vector3 position)
    {
        // Spawn bonus logic
        GameObject randomBonus = bonusTypes[Random.Range(0, bonusTypes.Length)];
        Instantiate(randomBonus, position, Quaternion.Euler(0, 0, 0));

        randomBonus.tag = "Ball";
    }

    public void RegisterCubeDestroyed(GameObject go)
    {
        // Calculate bonus chance
        if (bonusChance >= Random.Range(0, 100))
        {
            // Spaw bonus if chance is ok
            SpawnBonus(go.transform.position);
        }

        // Set changes, increment counter, update ui and destroy block
        _cubesDestroyed++;
        UpdateUI();
        Destroy(go);

        // Calculate destroyed blocks, maybe max score reached
        int maxBlocks = _blockSpawner.columns * _blockSpawner.rows;
        
        if (_cubesDestroyed == maxBlocks)
        {
            StartGame(true, "You Win!");
        }
    }

    public void RegisterBallDead(Ball ball)
    {
        // if bonus ball, just destroy and exit
        if (!ball.GetBallIsMain())
        {
            Destroy(ball.gameObject);
            return;
        }

        // decrease lives and refresh ui
        _lives--;
        UpdateUI();

        if (_lives <= 0)
        {
            // GameOver logic
            ball.gameObject.SetActive(false);
            
            // Show main menu with lose text only if last game was played by a player
            if(!_touchHandle.GetAIMode()) StartGame(true, "You lose!");
            return;
        }
        else
        {
            // just simple death logic
            // spawn ball as main
            SpawnBall(_playerBallPrefab, true);
            Destroy(ball.gameObject);
        }
    }

    // function without args, listens message from ui
    public void SpawnBonusBall()
    {
        SpawnBall(_playerBallPrefab, false);
    }

    public void TogglePause(Button btn)
    {
        // Pause toggle logic
        if (Time.timeScale == 0)
        {
            // Pause
            Time.timeScale = 1;
            btn.GetComponentInChildren<Text>().text = "Pause";
            _touchHandle.SetPauseState(false);
        }
        else
        {
            // Resume
            Time.timeScale = 0;
            btn.GetComponentInChildren<Text>().text = "Resume";
            _touchHandle.SetPauseState(true);
        }
    }

    public void ToggleLazyPlayerMode(Button btn)
    {
        if (_touchHandle.GetAIMode())
        {
            _touchHandle.SetAIMode(false);
            btn.GetComponentInChildren<Text>().text = "Lazy player on";
        }
        else
        {
            _touchHandle.SetAIMode(true);
            btn.GetComponentInChildren<Text>().text = "Lazy player off";
        }
    }
    
    private void SpawnBall(GameObject referenceBall, bool bIsMainBall = false)
    {
        // set offsets to avoid unexpected colliding
        Vector3 newPos = playerPlatform.transform.position;
        newPos.z += 2;
        newPos.y = 0.6f;
        
        // create instance
        GameObject newBall = Instantiate(referenceBall, newPos,Quaternion.Euler(0, 0, 0));

        // activate if clone made from inactive instance
        newBall.SetActive(true);
        
        // make ball flag as main or bonus
        if (bIsMainBall)
        {
            playerBall = newBall;
        }
        else
        {
            newBall.GetComponent<Ball>().SetMainBall(false);
        }
    }
}