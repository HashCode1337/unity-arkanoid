using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchHandle : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    // touch position
    private Vector2 _cursorPos = new Vector2(0,0);
    private bool _bIsTouchActual = false;
    
    // Border coordinate to avoid plate moving out from playground bounds
    private float _playgroundBorder;
    
    // Settings exposed in GameMaster
    private GameObject _playerPlate;
    private float _mouseSpeedMultiplier;

    // To prevent plate moving during pause
    private bool _bGamePaused;
    
    // plate controller is AI
    private bool _bAIMode = false;

    public bool GetAIMode()
    {
        return _bAIMode;
    }

    public void SetAIMode(bool bAIMode)
    {
        _bAIMode = bAIMode;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        _bIsTouchActual = true;
        _cursorPos = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        // ignore without touch or while pause
        // ignore if AI mode is active
        if (!_bIsTouchActual || _bAIMode) return;

        // set player pos var
        float playerPlatePos = _playerPlate.transform.position.x;
        
        // prepare main vars to operate with
        Vector2 direction = (_cursorPos - eventData.position).normalized * -1;
        float mouseSpeed = (_cursorPos - eventData.position).magnitude * _mouseSpeedMultiplier;

        // grab cursor actual pos and predict next frame pos
        _cursorPos = eventData.position;
        float plateSupposedPos = playerPlatePos + direction.x * Time.fixedDeltaTime * mouseSpeed;
        
        // mark flag if player wanna move out of playing area
        bool bShouldApplyPos = PositionInBorderRange(plateSupposedPos);

        // avoid moving player plate out from playing area
        if (bShouldApplyPos)
            _playerPlate.transform.Translate(direction.x * Time.fixedDeltaTime * mouseSpeed, 0, 0);

    }

    private bool PositionInBorderRange(float xPos)
    {
        // mark flag if player wanna move out of playing area
        bool bShouldApplyPos = true;
        if (xPos >= _playgroundBorder) bShouldApplyPos = false;
        else if (xPos <= -_playgroundBorder) bShouldApplyPos = false;
        else if (_bGamePaused) bShouldApplyPos = false;

        return bShouldApplyPos;
    }

    private GameObject FindNearestBall(out bool bFound)
    {
        // All balls is scene
        GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
        
        // Vars we should use to store data about ball
        float nearestBallDistance = 100000.0f;
        GameObject nearestBall = null;

        // Algorithm to search nearest ball
        foreach (GameObject ball in balls)
        {
            float curElementDistance = ball.transform.position.z - _playerPlate.transform.position.z;
            
            if (curElementDistance < nearestBallDistance)
            {
                nearestBallDistance = curElementDistance;
                nearestBall = ball;
            }
        }
    
        // Assign value to out boolean
        if (balls.Length <= 0) bFound = false;
        else bFound = true;

        // Return ball or null if ball hasn't been found
        return nearestBall;
    }

    private void ChaseBallAI()
    {
        
        GameObject ball = FindNearestBall(out bool bIsBallExist);
        if (bIsBallExist)
        {
            // Set poses in vars
            Vector3 bPos = ball.transform.position;
            Vector3 pPos = _playerPlate.transform.position;
            
            // Debug show current ai target
            Debug.DrawLine(pPos, bPos,Color.red);

            // Calc next frame plate desired position
            Vector3 desiredPos = pPos + (new Vector3(bPos.x, pPos.y, pPos.z) - pPos).normalized * Time.fixedDeltaTime * 15;

            // Apply position if it's inside of playing area bounds
            if (PositionInBorderRange(desiredPos.x)) _playerPlate.transform.position = desiredPos;
        }
    }
    
    void FixedUpdate()
    {
        if (_bAIMode) ChaseBallAI();
    }

    public void SetPauseState(bool bIsPaused)
    {
        _bGamePaused = bIsPaused;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        _bIsTouchActual = false;
    }

    public void InitSettings(GameObject deathWall, GameObject playerPlatform, float mouseSpeedMultiplier)
    {
        _mouseSpeedMultiplier = mouseSpeedMultiplier;
        _playerPlate = playerPlatform;
        _playgroundBorder = (deathWall.transform.localScale.x - playerPlatform.transform.localScale.x) / 2.1f;
    }
}
